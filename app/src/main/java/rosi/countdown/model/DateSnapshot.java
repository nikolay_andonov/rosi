package rosi.countdown.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by nikolay.andonov on 11.11.17.
 */

@IgnoreExtraProperties
public class DateSnapshot {

    public int year;
    public int month;
    public int day;
    public int hours;
    public int minutes;
    public int seconds;

    public DateSnapshot() {

    }

    public DateSnapshot(int year, int month, int day, int hours, int minutes, int seconds) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
}
