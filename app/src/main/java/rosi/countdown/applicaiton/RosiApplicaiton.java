package rosi.countdown.applicaiton;

import android.app.Application;
import android.content.Context;

/**
 * Created by nikolay.andonov on 14.11.17.
 */

public class RosiApplicaiton extends Application {

    public static Context getSharedApplicationContext() {
        return sharedApplicationContext;
    }

    private static Context sharedApplicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedApplicationContext = this;
    }

}
