package rosi.countdown.controllers;


import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import rosi.countdown.model.DateSnapshot;

/**
 * Created by nikolay.andonov on 11.11.17.
 */

public class FirebaseController {

    private static final String START_DATE_KEY = "startDate";
    private static final String END_DATE_KEY = "endDate";
    private static final String LOG_TAG = "FIREBASE_CONTROLLER";


    public interface UpdatedDateInterface {
        public void didUpdateEndDate(DateSnapshot endDate);
        public void didUpdateStartDate(DateSnapshot endDate);
    }
    private DatabaseReference mDateReference;

    public FirebaseController(final UpdatedDateInterface updateDateInterface) {
       mDateReference = FirebaseDatabase.getInstance().getReference();


        ValueEventListener endDateListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DateSnapshot dateSnapshot = dataSnapshot.getValue(DateSnapshot.class);
                updateDateInterface.didUpdateEndDate(dateSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FIREBASE_CONTROLLER", databaseError.getDetails());
            }
        };
        mDateReference.child(END_DATE_KEY).addValueEventListener(endDateListener);

        ValueEventListener startDateListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DateSnapshot dateSnapshot = dataSnapshot.getValue(DateSnapshot.class);
                updateDateInterface.didUpdateStartDate(dateSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FIREBASE_CONTROLLER", databaseError.getDetails());
            }
        };
        mDateReference.child(START_DATE_KEY).addValueEventListener(startDateListener);

    }


}
