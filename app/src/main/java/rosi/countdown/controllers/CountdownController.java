package rosi.countdown.controllers;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.Seconds;

import java.util.Timer;
import java.util.TimerTask;

import rosi.countdown.Manifest;
import rosi.countdown.model.DateSnapshot;

/**
 * Created by nikolay.andonov on 9.11.17.
 */

public class CountdownController implements FirebaseController.UpdatedDateInterface {

    public interface CountdownInterface{
        void didUpdateRemainingSeconds(int seconds);
        void didFinishCountingDown();
        void didUpdateProgress(int value);
    }

    private DateTime endDateTime;
    private DateTime startDateTime;
    private Timer timer;
    private Context context;
    private Handler mainHandler;
    private CountdownInterface countdownInterface;
    private int remainingSeconds;
    private int totalSecondsFromStart;
    private Seconds totalSecondsInterval;

    public CountdownController(Context context, CountdownInterface countdownInterface) {

        FirebaseController firebaseController = new FirebaseController(this);

        this.context = context;
        mainHandler = new Handler(context.getMainLooper());
        this.countdownInterface = countdownInterface;
    }

    private void setupTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        didUpdateTimerValue();
                        remainingSeconds--;
                        if(remainingSeconds == 0) {
                            timer.cancel();
                            didFinishUpdatingTimerValue();
                        }
                    }
                });
            }
        }, 0,1000);
    }

    private void didUpdateTimerValue() {
        tryUpdateProgress();
      countdownInterface.didUpdateRemainingSeconds(remainingSeconds);
    }

    private void didFinishUpdatingTimerValue() {
        countdownInterface.didFinishCountingDown();
    }

    @Override
    public void didUpdateEndDate(DateSnapshot endDate) {

        endDateTime = new DateTime(endDate.year,
                endDate.month,
                endDate.day,
                endDate.hours,
                endDate.minutes,
                endDate.seconds, 0);
        if(startDateTime != null) {
            totalSecondsInterval = Seconds.secondsBetween(startDateTime, endDateTime);
            totalSecondsFromStart = totalSecondsInterval.getSeconds();
        }
        remainingSeconds = calculateRemainingSeconds();
        if(timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        if (remainingSeconds <= 0) {
            didFinishUpdatingTimerValue();
        } else {
            setupTimer();
        }

        tryUpdateProgress();
    }

    @Override
    public void didUpdateStartDate(DateSnapshot endDate) {
        startDateTime = new DateTime(endDate.year,
                endDate.month,
                endDate.day,
                endDate.hours,
                endDate.minutes,
                endDate.seconds, 0);
        if(endDateTime != null) {
            totalSecondsInterval = Seconds.secondsBetween(startDateTime, endDateTime);
            totalSecondsFromStart = totalSecondsInterval.getSeconds();
        }
        tryUpdateProgress();
    }

    private int calculateRemainingSeconds() {
        LocalDateTime nowLocal = new LocalDateTime();
        DateTime nowUTC = nowLocal.toDateTime(DateTimeZone.getDefault());
        Seconds seconds = Seconds.secondsBetween(nowUTC, endDateTime);
        return seconds.getSeconds();
    }

    private void tryUpdateProgress() {
        if(startDateTime == null || endDateTime == null) {
            return;
        }

        int elapsedSeconds = totalSecondsFromStart - remainingSeconds;

        double progress = ((double) elapsedSeconds/ totalSecondsFromStart) * 100;
        double validatedProgress = Math.min(100.0, Math.max(0.0, progress));

        countdownInterface.didUpdateProgress((int) validatedProgress);
    }

}
