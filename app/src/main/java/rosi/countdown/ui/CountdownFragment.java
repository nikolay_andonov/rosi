package rosi.countdown.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import cn.fanrunqi.waveprogress.WaveProgressView;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import rosi.countdown.R;
import rosi.countdown.applicaiton.RosiApplicaiton;
import rosi.countdown.base.BaseFragment;
import rosi.countdown.controllers.CountdownController;

/**
 * Created by nikolay.andonov on 14.11.17.
 */

public class CountdownFragment extends BaseFragment implements CountdownController.CountdownInterface, View.OnTouchListener {

    private TextView countdownLabel;
    private WaveProgressView progressView;
    private int lastRecordedProgress = 0;
    private KonfettiView konfettiView;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_countdown;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        countdownLabel = view.findViewById(R.id.countdown_label);
        konfettiView = view.findViewById(R.id.konfetti_view);
        konfettiView.bringToFront();

        progressView = view.findViewById(R.id.progressView);
        progressView.setCurrent(0, "");
        progressView.setMaxProgress(100);
        progressView.setWaveColor("#ef99b7");
        progressView.setmWaveSpeed(10);

        konfettiView.setOnTouchListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CountdownController(RosiApplicaiton.getSharedApplicationContext(), this);
    }

    @Override
    public void didUpdateRemainingSeconds(int seconds) {
        countdownLabel.setText(String.valueOf(seconds));
    }

    @Override
    public void didFinishCountingDown() {
        countdownLabel.setText("No more waiting");
    }

    @Override
    public void didUpdateProgress(int value) {

        if(value < lastRecordedProgress) {
            //Refreshing the animation
            progressView.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);
        }
        progressView.setCurrent(value, "");
        lastRecordedProgress = value;
    }

    private void triggerKonfettiBurst(int x, int y) {

        Size sizeSmall = new Size(12, 5);
        Size sizeBig = new Size(16, 6);

        konfettiView.build()
                .addColors(getResources().getColor(R.color.colorAccent), Color.WHITE, Color.parseColor("#8e5b9b"), Color.parseColor("#ef6ee0"))
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(1500L)
                .addShapes(Shape.CIRCLE)
                .setPosition(x, y)
                .addSizes(sizeSmall, sizeBig)
                .burst(75);
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.i("TAG", "touched down");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.i("TAG", "moving: (" + x + ", " + y + ")");
                break;
            case MotionEvent.ACTION_UP:
                Log.i("TAG", "touched up");
                break;
        }

        triggerKonfettiBurst(x,y);
        return true;
    }
}
