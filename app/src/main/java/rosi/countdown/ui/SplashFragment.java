package rosi.countdown.ui;

import android.animation.Animator;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;

import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rosi.countdown.R;
import rosi.countdown.base.BaseFragment;

/**
 * Created by nikolay.andonov on 14.11.17.
 */

public class SplashFragment extends BaseFragment implements View.OnClickListener {

    private ViewGroup rosiViewGroup;
    private LottieAnimationView rView;
    private LottieAnimationView oView;
    private LottieAnimationView sView;
    private LottieAnimationView iView;
    private LottieAnimationView containerView;


    //0 for the very first initialization attempt
    private int currentAnimationIndex = 0;
    private int firstAnimationIndex = 1;
    private int maxAnimationIndex = 6;

    private Timer timer;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rosiViewGroup = (ViewGroup) view.findViewById(R.id.rosi_view_group);
        containerView = (LottieAnimationView) view.findViewById(R.id.animation_container);
        containerView.setOnClickListener(this);
        containerView.bringToFront();
        containerView.addColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN));


        rView = (LottieAnimationView) view.findViewById(R.id.animation_view_r);
        oView = (LottieAnimationView) view.findViewById(R.id.animation_view_o);
        sView = (LottieAnimationView) view.findViewById(R.id.animation_view_s);
        iView = (LottieAnimationView) view.findViewById(R.id.animation_view_i);

        final Handler mainHandler = new Handler(getActivity().getMainLooper());
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        rView.playAnimation();
                        oView.playAnimation();
                        sView.playAnimation();
                        iView.playAnimation();
                    }
                });
            }
        }, 0, 5000);

        renderNextAnimation();
        containerView.playAnimation();
    }

    private void renderNextAnimation() {
        currentAnimationIndex++;
        if (currentAnimationIndex > maxAnimationIndex) {
            currentAnimationIndex = firstAnimationIndex;
        }

        String animationIndex = String.valueOf(currentAnimationIndex) + ".json";
        containerView.setAnimation(animationIndex);

        containerView.loop(true);
        containerView.playAnimation();
    }

    @Override
    public void onClick(View view) {
        containerView.setOnClickListener(null);
        containerView.animate().alpha(0).setDuration(1000).setInterpolator(new DecelerateInterpolator()).withEndAction(new Runnable() {
            @Override
            public void run() {
                renderNextAnimation();
                containerView.animate().alpha(1).setDuration(1000).setInterpolator(new AccelerateInterpolator()).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        if (currentAnimationIndex == 1 || currentAnimationIndex == 2) {
                            containerView.addColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN));
                        } else {
                            containerView.clearColorFilters();
                        }
                        containerView.setOnClickListener(SplashFragment.this);
                    }
                }).start();
            }
        }).start();
    }


}
