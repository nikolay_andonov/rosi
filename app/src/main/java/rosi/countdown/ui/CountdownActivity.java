package rosi.countdown.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.fanrunqi.waveprogress.WaveProgressView;
import rosi.countdown.R;
import rosi.countdown.controllers.CountdownController;

public class CountdownActivity extends Activity implements CountdownController.CountdownInterface {

    private TextView countdownLabel;
    private WaveProgressView progressView;
    private int lastRecordedProgress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_countdown);
        countdownLabel = findViewById(R.id.countdown_label);

        progressView = findViewById(R.id.progressView);
        progressView.setCurrent(0, "");
        progressView.setMaxProgress(100);
        progressView.setWaveColor("#ef99b7");
        progressView.setmWaveSpeed(10);

        new CountdownController(this, this);
    }

    @Override
    public void didUpdateRemainingSeconds(int seconds) {
        countdownLabel.setText(String.valueOf(seconds));
    }

    @Override
    public void didFinishCountingDown() {
        countdownLabel.setText("No more waiting");
    }

    @Override
    public void didUpdateProgress(int value) {

        if(value < lastRecordedProgress) {
            //Refreshing the animation
            progressView.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);
        }
        progressView.setCurrent(value, "");
        lastRecordedProgress = value;
    }
}
