package rosi.countdown.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rosi.countdown.ui.CountdownFragment;
import rosi.countdown.ui.SplashFragment;

/**
 * Created by nikolay.andonov on 14.11.17.
 */

public class RootViewPagerAdapter extends FragmentPagerAdapter {

    final private int SPLASH_FRAGMENT_INDEX = 0;
    final private int COUNTDOWN_FRAGMENT_INDEX = 1;

    public RootViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case SPLASH_FRAGMENT_INDEX: {
                return new SplashFragment();
            }
            case COUNTDOWN_FRAGMENT_INDEX: {
                return new CountdownFragment();
            }
            default: {
                return null;
            }
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
