package rosi.countdown.ui;

import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import rosi.countdown.R;
import rosi.countdown.ui.adapters.RootViewPagerAdapter;

/**
 * Created by nikolay.andonov on 14.11.17.
 */

public class RootActivity extends FragmentActivity {

    private ViewPager rootViewPager;
    private RootViewPagerAdapter rootViewPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        rootViewPager = (ViewPager) findViewById(R.id.view_pager);
        rootViewPagerAdapter = new RootViewPagerAdapter(getSupportFragmentManager());
        rootViewPager.setAdapter(rootViewPagerAdapter);

    }
}
